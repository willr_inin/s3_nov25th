<?xml version="1.0" encoding="UTF-8"?>
<!--
  $File: //edge/main_systest/pub/resources/documents/ccxml/line_incoming.ccxml $
  $Revision: #7 $
  $Change: 923703 $
  $DateTime: 2013/11/15 14:40:52 $
  $Author: richardn $

  Copyright (c) Interactive Intelligence, Inc. 2012
  All Rights Reserved
 -->
<ccxml version="1.0">

    <!-- line_incoming.ccxml
            Purpose:
                Process an incoming call and share signaling with a created outgoing call. The
                incomingConnection is the primary resource of this session document and is the
                driver of all events to the cloud.

            Session Variables:
                None.

            Operations:
                cloud.command.disconnect    : Disconnects the incoming connection, response disconnect event disconnects the bridge which will then invoke the common disconnect handler
     -->

    <var name="incomingConnection" expr="'0'"/>         <!-- Incoming connection -->
    <var name="incomingConnectionProxy" expr="'0'"/>    <!-- Proxy to represent the media endpoint of the incoming connection -->
    <var name="incomingConnectionProxyHints" expr=""/>  <!-- Hints used when joining to incomingConnectionProxy -->
    <var name="bridgeConnection" expr="'0'"/>           <!-- Bridge connection -->
    <var name="bridgeTransferConnection" expr="'0'"/>   <!-- Bridge Transfer connection, active while the transfer request is active -->
    <var name="oldBridgeTransferConnection" expr="'0'"/><!-- Bridge Transfer connection that is no longer active because it was disconnected and replaced -->
    <var name="speakToTransferConnection" expr="'0'"/>  <!-- SpeakTo Transfer connection, remains active until last connection disconnects -->
    <var name="speakToConference" expr="'0'"/>          <!-- Conference object id for the speakTo feature -->
    <var name="dialogHold" expr="'0'"/>                 <!-- Dialog endpoint id hold audio -->
    <var name="sendTarget" expr="'0'"/>                 <!-- Target Connection for all cloud send elements -->
    <var name="aai" expr=""/>                           <!-- Line party's AAI -->
    <var name="speakTo" expr="'both'"/>                 <!-- SpeakTo transfer signal's last speakTo value -->
    <var name="dialogRecord" expr="'0'"/>               <!-- Dialog endpoint id record call -->
    <var name="direction" expr="'inbound'"/>            <!-- Cloud payload name: Direction of primary connection - constant -->
    <var name="state" expr="'offering'"/>               <!-- Cloud payload name: State of session - initially in offering state -->
    <var name="disconnectType" expr=""/>                <!-- Cloud payload name: Type of the disconnect -->
    <var name="audio" expr="({status: 'full'})"/>       <!-- Maintains state of the audio status combinations: none, mute, deafen, full -->
    <assign name="audio.hold" expr="({remote: false, local: false})"/> <!-- Maintains state of the audio hold -->


    <eventprocessor>

        <!-- Incoming Connection Handlers -->
        <transition event="connection.alerting" cond="incomingConnection == '0'">
            <!-- Receive the incoming connection and notify the cloud -->
            <log expr="'incoming: connection.alerting for ' + event$.connectionid"/>
            <assign name="incomingConnection" expr="event$.connectionid"/>
            <assign name="incomingConnectionProxy" expr="incomingConnection"/>
            <assign name="sendTarget" expr="event$.connectionid"/>
            <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
            <!-- Create an outgoing session bridge connection to connect the incoming call to -->
            <createcall connectionid="bridgeConnection"
                        hints="({dialplan: 'tieline'})"
                        dest="event$.connection.local"
                        callerid="event$.connection.remote"
                        joinid="incomingConnection"
            />
            <log expr="'created outgoing session bridge call ' + bridgeConnection"/>
        </transition>

        <transition event="connection.signal.transfer" cond="event$.connectionid == incomingConnection">
            <log expr="'incoming: connection.signal.transfer for ' + event$.connectionid"/>
            <log expr="'transfer request from an external source is not supported'"/>
        </transition>

        <transition event="connection.connected" cond="event$.connectionid == incomingConnection">
            <log expr="'incoming: connection.connected for ' + event$.connectionid"/>
            <assign name="state" expr="'connected'"/>
            <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
            <!-- Query line config to determine if recording is enabled -->
            <var name="line" expr="Edge.get_line(session.connections[incomingConnection]._line);"/>
            <var name="lineRecordingEnabled" expr="line.get_attribute('line.recording.enabled', new AttributeValue(false))"/>
            <if cond="lineRecordingEnabled.valueOf() == true">
                <log expr="'lineRecordingEnabled: createconference and initiate line recording'"/>
                <var name="lineRecordingAudioFormat" expr="line.get_attribute('line.recording.audioFormat', new AttributeValue('audio/pcmu'))"/>
                <var name="lineRecordingLevelControlEnabled" expr="line.get_attribute('line.recording.levelControlEnabled', new AttributeValue(true))"/>
                <var name="lineRecordingCryptoSuite" expr="line.get_attribute('line.recording.cryptoSuite', new AttributeValue('NULL_MD5'))"/>
                <var name="lineRecordingPublicKey" expr="line.get_attribute('line.recording.publicKey', new AttributeValue(''))"/>
                <!-- Setup the monitor as the proxy for incoming connection -->
                <createconference conferenceid="incomingConnectionProxy" hints="({type:'tap'})"/>
                <join id1="incomingConnection" id2="incomingConnectionProxy" duplex="'full'" hints="({join: {id2: {pin: 'A'}}})"/>
                <join id1="bridgeConnection" id2="incomingConnectionProxy" duplex="'full'" hints="({join: {id2: {pin: 'B'}}})"/>
                <assign name="incomingConnectionProxyHints" expr="({join: {id2: {pin: 'B'}}})"/>
                <!-- Start the recorder and join to monitor -->
                <dialogstart dialogid="dialogRecord"
                             src="'x-inin-recorder:record'"
                             type="lineRecordingAudioFormat.valueOf()"
                             conferenceid="incomingConnectionProxy"
                             mediadirection="dialogreceive"
                             hints="({join: {id2: {pin: 'monitor'}},
                                      resource: {id1: {params: {'mediaprovider.recordingCryptoSuite': lineRecordingCryptoSuite.valueOf(),
                                                                'mediaprovider.recordingPublicKey': lineRecordingPublicKey.valueOf(),
                                                                'mediaprovider.levelControlEnabled': lineRecordingLevelControlEnabled.valueOf()}}}})"/>
            </if>
        </transition>

        <transition event="connection.disconnected" cond="event$.connectionid == incomingConnection">
            <log expr="'incoming: connection.disconnected for ' + event$.connectionid"/>
            <if cond="speakToTransferConnection != '0' &amp;&amp; bridgeConnection != '0'">
                <!-- incoming connection dropping out, return to special 2 party scenario bridged through this session -->
                <assign name="incomingConnection" expr="'0'"/>
            <else/>
                <!-- If type is already set do not overwrite its value, it was set by the cloud disconnect -->
                <if cond="!disconnectType">
                    <assign name="disconnectType" expr="'explicit.endpoint'"/>
                </if>
                <!-- Provide disconnected state -->
                <assign name="state" expr="'disconnected'"/>
                <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType"/>
                <!-- Disconnect the session bridge connection -->
                <disconnect connectionid="bridgeConnection" hints="event$.info"/>
                <!-- Clear both connections and allow common disconnect handler to complete document -->
                <assign name="incomingConnection" expr="'0'"/>
                <assign name="bridgeConnection" expr="'0'"/>
            </if>
            <!-- Terminate line recording if it is active -->
            <if cond="dialogRecord != '0'">
                <dialogterminate dialogid="dialogRecord" immediate="false"/>
            </if>
        </transition>

        <transition event="connection.failed" cond="event$.connectionid == incomingConnection">
            <log expr="'incoming: connection.failed for ' + event$.connectionid"/>
            <!-- Provide disconnected state then move immediately to a terminated state -->
            <assign name="state" expr="'disconnected'"/>
            <assign name="disconnectType" expr="'explicit.error'"/>
            <var name="errorInfo" expr="({text: event$.reason})"/>
            <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType errorInfo"/>
            <!-- Disconnect the session bridge connection -->
            <disconnect connectionid="bridgeConnection" hints="event$.info"/>
            <!-- Clear all connections and allow common disconnect handler to complete document -->
            <assign name="incomingConnection" expr="'0'"/>
            <assign name="bridgeConnection" expr="'0'"/>
            <assign name="speakToTransferConnection" expr="'0'"/>
            <!-- Terminate line recording if it is active -->
            <if cond="dialogRecord != '0'">
                <dialogterminate dialogid="dialogRecord" immediate="false"/>
            </if>
        </transition>

        <!-- Bridge Connection Handlers -->
        <transition event="connection.progressing" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.progressing for ' + event$.connectionid"/>
            <if cond="session.connections[incomingConnection].state != 2"> 
                <if cond="event$.connection.substate">
                    <!-- Substate provided so relay platform for incoming call -->
                    <accept connectionid="incomingConnection" hints="event$.connection.substate"/>
                </if>
            </if>
        </transition>

        <transition event="connection.signal.transfer" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.signal.transfer for ' + event$.connectionid"/>
            <!-- Data for the new transfer call -->
            <if cond="event$.info.aai != undefined">
                <assign name="aai" expr="event$.info.aai"/>
            </if>
            <!-- Branch functionality based on type of transfer -->
            <if cond="event$.info.speakTo != undefined">
                <var name="newSpeakToConnection" expr="false"/>
                <if cond="speakToConference == '0'">
                    <assign name="newSpeakToConnection" expr="true"/>          
                    <createcall connectionid="speakToTransferConnection"
                                hints="({dialplan: 'tieline', diversion: {address: session.connections[bridgeConnection].remote}})"
                                dest="event$.info.destination.address"
                                callerid="session.connections[incomingConnection].remote"
                                aai="aai"
                    />
                    <createconference confname="'speakTo-' + speakToTransferConnection" conferenceid="speakToConference" reservedtalkers="3"/>
                    <join id1="speakToConference" id2="incomingConnection" duplex="'half'"/>  <!-- TODO: Keep this until bug with acquire_proxies() is fixed -->
                    <unjoin id1="speakToConference" id2="incomingConnection"/>                <!-- TODO: Keep this until bug with acquire_proxies() is fixed -->
                    <unjoin id1="bridgeConnection" id2="incomingConnectionProxy" hints="incomingConnectionProxyHints"/>   <!-- Workaround until TapProxy is fixed -->
                </if>
                <!-- clear the previous speakto action -->
                <if cond="speakTo == 'a'">
                    <redirect connectionid="speakToTransferConnection" dest="session.connections[speakToTransferConnection].remote" hints="({hold: false})"/>
                <elseif cond="speakTo == 'b'"/>
                    <if cond="dialogHold != '0'">
                        <dialogterminate dialogid="dialogHold" immediate="true"/>
                    </if>
                    <assign name="audio.hold.remote" expr="false"/>
                    <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
                </if>
                <assign name="speakTo" expr="event$.info.speakTo"/>
                <!-- if incoming connection is disconnected then this is a special 2-party conference and speakto is not allowed -->
                <if cond="incomingConnection != '0'">
                    <!-- apply the new speakto actions -->
                    <if cond="speakTo == 'a'">
                        <unjoin id1="speakToConference" id2="incomingConnectionProxy" hints="incomingConnectionProxyHints"/>  <!-- Workaround until TapProxy is fixed -->
                        <unjoin id1="speakToConference" id2="speakToTransferConnection"/>                                                   <!-- take speakTo (B) out of the conference -->
                        <join id1="speakToConference" id2="incomingConnectionProxy" duplex="'full'" hints="incomingConnectionProxyHints"/>  <!-- add incoming (A) into the conference -->
                        <join id1="speakToConference" id2="bridgeConnection" duplex="'full'"/>                                              <!-- add bridge (C) into the conference -->
                        <!-- put speakTo (B) on hold -->
                        <if cond="newSpeakToConnection == false">
                            <redirect connectionid="speakToTransferConnection" dest="session.connections[speakToTransferConnection].remote" hints="({hold: true})"/>
                        </if>
                    <elseif cond="speakTo == 'b'"/>
                        <unjoin id1="speakToConference" id2="incomingConnectionProxy" hints="incomingConnectionProxyHints"/>    <!-- take incoming (A) out of the conference -->
                        <join id1="speakToConference" id2="speakToTransferConnection" duplex="'full'"/>                         <!-- add speakTo (B) into the conference -->
                        <join id1="speakToConference" id2="bridgeConnection" duplex="'full'"/>                                  <!-- add bridge (C) into the conference -->
                        <!-- put incoming (A) on hold which is this document -->
                        <assign name="audio.hold.remote" expr="true"/>
                        <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
                        <var name="line" expr="Edge.get_line(session.connections[incomingConnection]._line);"/>
                        <var name="srcAttr" expr="line.get_attribute('line.resource.hold')"/>
                        <if cond="!(srcAttr.isNull || srcAttr.empty)">
                            <dialogstart src="'data:text/uri-list,' + 'x-inin-audiosrc:control/loop_begin%0D%0A' + srcAttr.valueOf() + '%0D%0Ax-inin-audiosrc:control/loop_end'"
                                         type="'audio/x-wav'"
                                         dialogid="dialogHold"
                                         mediadirection="both"
                            />
                            <join id1="dialogHold" id2="incomingConnectionProxy" duplex="'full'" hints="incomingConnectionProxyHints"/>
                        <else/>
                            <log expr="'remotely held without hold audio - line.resource.hold is not configured for ' + session.connections[incomingConnection]._line"/>
                            <unjoin id1="bridgeConnection" id2="incomingConnectionProxy" hints="incomingConnectionProxyHints"/>
                        </if>
                    <else/>
                        <unjoin id1="speakToConference" id2="incomingConnectionProxy" hints="incomingConnectionProxyHints"/>  <!-- Workaround until TapProxy is fixed -->
                        <join id1="speakToConference" id2="incomingConnectionProxy" duplex="'full'" hints="incomingConnectionProxyHints"/>  <!-- incoming (A) full party of conference -->
                        <join id1="speakToConference" id2="speakToTransferConnection" duplex="'full'"/>                                     <!-- speakTo (B) full party of conference -->
                        <join id1="speakToConference" id2="bridgeConnection" duplex="'full'"/>                                              <!-- bridge (C) full party of conference -->
                    </if>
                <elseif cond="speakTo == 'both'"/>
                    <join id1="speakToConference" id2="speakToTransferConnection" duplex="'full'"/>     <!-- speakTo (B) full party of conference -->
                    <join id1="speakToConference" id2="bridgeConnection" duplex="'full'"/>              <!-- bridge (C) full party of conference -->
                </if>
            <elseif cond="speakToConference != '0'"/>
                <!-- no operation handler, transfers are not allowed once in 3-party state -->
            <else/>
                <if cond="bridgeTransferConnection != '0'">
                    <!-- already an active transfer, disconnect it and start a new one -->
                    <disconnect connectionid="bridgeTransferConnection"/>
                    <assign name="oldBridgeTransferConnection" expr="bridgeTransferConnection"/>
                </if>
                <createcall connectionid="bridgeTransferConnection"
                            hints="({dialplan: 'tieline', diversion: {address: session.connections[bridgeConnection].remote}})"
                            dest="event$.info.destination.address"
                            callerid="session.connections[incomingConnection].remote"
                            aai="aai"
                />
            </if>
        </transition>

        <transition event="connection.signal.hold" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.signal.hold = ' + event$.info.state + ' for ' + event$.connectionid"/>
            <assign name="audio.hold.remote" expr="event$.info.state"/>
            <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
            <!-- Alter the hold audio to line connection -->
            <if cond="event$.info.state">
                <var name="line" expr="Edge.get_line(session.connections[incomingConnection]._line);"/>
                <var name="srcAttr" expr="line.get_attribute('line.resource.hold')"/>
                <if cond="!(srcAttr.isNull || srcAttr.empty)">
                    <dialogstart src="'data:text/uri-list,' + 'x-inin-audiosrc:control/loop_begin%0D%0A' + srcAttr.valueOf() + '%0D%0Ax-inin-audiosrc:control/loop_end'"
                                 type="'audio/x-wav'"
                                 dialogid="dialogHold"
                                 mediadirection="both"
                    />
                    <unjoin id1="bridgeConnection" id2="incomingConnectionProxy" hints="incomingConnectionProxyHints"/>  <!-- Workaround until TapProxy is fixed -->
                    <join id1="dialogHold" id2="incomingConnectionProxy" duplex="'full'" hints="incomingConnectionProxyHints"/>
                <else/>
                    <log expr="'remotely held without hold audio - line.resource.hold is not configured for ' + session.connections[incomingConnection]._line"/>
                    <unjoin id1="bridgeConnection" id2="incomingConnectionProxy" hints="incomingConnectionProxyHints"/>
                </if>
            <else/>
                <unjoin id1="dialogHold" id2="incomingConnectionProxy" hints="incomingConnectionProxyHints"/>  <!-- Workaround until TapProxy is fixed -->
                <join id1="bridgeConnection" id2="incomingConnectionProxy" duplex="'full'" hints="incomingConnectionProxyHints"/>
                <if cond="dialogHold != '0'">
                    <dialogterminate dialogid="dialogHold" immediate="true"/>
                </if>
            </if>
        </transition>

        <transition event="connection.connected" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.connected for ' + event$.connectionid"/>
            <if cond="session.connections[incomingConnection].state != 2"> 
                <accept connectionid="incomingConnection"/>
            </if>
        </transition>

        <transition event="connection.disconnected" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.disconnected for ' + event$.connectionid"/>
            <if cond="speakToTransferConnection != '0' &amp;&amp; incomingConnection != '0'">
                <!-- bridge connection dropping out, return to normal 2 party session (swapping bridge and clearing holds) -->
                <if cond="speakTo == 'a'">
                    <redirect connectionid="speakToTransferConnection" dest="session.connections[speakToTransferConnection].remote" hints="({hold: false})"/>
                <elseif cond="speakTo == 'b'"/>
                    <if cond="dialogHold != '0'">
                        <dialogterminate dialogid="dialogHold" immediate="true"/>
                    </if>
                    <assign name="audio.hold.remote" expr="false"/>
                    <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
                </if>
                <unjoin id1="speakToConference" id2="incomingConnectionProxy" hints="incomingConnectionProxyHints"/>  <!-- Workaround until TapProxy is fixed -->
                <join id1="speakToTransferConnection" id2="incomingConnectionProxy" duplex="'full'" hints="incomingConnectionProxyHints"/>
                <destroyconference conferenceid="speakToConference"/>
                <assign name="speakToConference" expr="'0'"/>
                <assign name="bridgeConnection" expr="speakToTransferConnection"/>
                <assign name="speakToTransferConnection" expr="'0'"/>
            <elseif cond="bridgeTransferConnection != '0'"/>
                <!-- Original bridge has been disconnected during active transfer, complete the transfer -->
                <join id1="bridgeTransferConnection" id2="incomingConnection" duplex="'full'"/>
                <assign name="bridgeConnection" expr="bridgeTransferConnection"/>
                <assign name="bridgeTransferConnection" expr="'0'"/>
            <else/>
                <!-- Provide disconnected state -->
                <assign name="state" expr="'disconnected'"/>
                <assign name="disconnectType" expr="'implicit'"/>
                <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType"/>
                <!-- Only one connection left, start the process of ending the session -->
                <assign name="bridgeConnection" expr="'0'"/>
                <if cond="incomingConnection != '0'">
                    <disconnect connectionid="incomingConnection" hints="event$.info"/>
                    <assign name="incomingConnection" expr="'0'"/>
                <else/>
                    <disconnect connectionid="speakToTransferConnection" hints="event$.info"/>
                    <assign name="speakToTransferConnection" expr="'0'"/>
                </if>
                <!-- Terminate line recording if it is active -->
                <if cond="dialogRecord != '0'">
                    <dialogterminate dialogid="dialogRecord" immediate="false"/>
                </if>
            </if>
        </transition>

        <transition event="connection.failed" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.failed for ' + event$.connectionid"/>
            <!-- Provide disconnected state then move immediately to a terminated state -->
            <assign name="state" expr="'disconnected'"/>
            <assign name="disconnectType" expr="'explicit.error'"/>
            <var name="errorInfo" expr="({text: event$.reason})"/>
            <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType errorInfo"/>
            <!-- Disconnect the incoming connection -->
            <disconnect connectionid="incomingConnection" hints="event$.info"/>
            <!-- Clear all connections and allow common disconnect handler to complete document -->
            <assign name="incomingConnection" expr="'0'"/>
            <assign name="bridgeConnection" expr="'0'"/>
            <assign name="speakToTransferConnection" expr="'0'"/>
            <!-- Terminate line recording if it is active -->
            <if cond="dialogRecord != '0'">
                <dialogterminate dialogid="dialogRecord" immediate="false"/>
            </if>
        </transition>

        <!-- Bridge Transfer Connection Handlers -->
        <transition event="connection.connected" cond="event$.connectionid == bridgeTransferConnection">
            <log expr="'bridge transfer: connection.connected for ' + event$.connectionid"/>
            <!-- Signal success by joining up the new audio and replacing the original bridge with the transfer bridge -->
            <join id1="bridgeTransferConnection" id2="incomingConnectionProxy" duplex="'full'" hints="incomingConnectionProxyHints"/>
            <if cond="bridgeConnection != '0'">
                <disconnect connectionid="bridgeConnection" hints="({sip: {code: 410}})"/>
            </if>
        </transition>

        <transition event="connection.disconnected" cond="event$.connectionid == bridgeTransferConnection">
            <log expr="'bridge transfer: connection.disconnected for ' + event$.connectionid"/>
            <!-- Transfer failed, clear the transfer connection -->
            <assign name="bridgeTransferConnection" expr="'0'"/>
            <if cond="bridgeConnection == '0'">
                <!-- Originating connection has already disconnected, need to end session -->
                <assign name="state" expr="'disconnected'"/>
                <assign name="disconnectType" expr="'implicit'"/>
                <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType"/>
                <!-- Disconnect the incoming connection -->
                <disconnect connectionid="incomingConnection" hints="event$.info"/>
                <!-- Clear connection and allow common disconnect handler to complete document -->
                <assign name="incomingConnection" expr="'0'"/>
                <!-- Terminate line recording if it is active -->
                <if cond="dialogRecord != '0'">
                    <dialogterminate dialogid="dialogRecord" immediate="false"/>
                </if>
            </if>
        </transition>

        <!-- Old Bridge Transfer Connection Handlers -->
        <transition event="connection.disconnected" cond="event$.connectionid == oldBridgeTransferConnection">
            <log expr="'old bridge transfer: connection.disconnected for ' + event$.connectionid"/>
            <assign name="oldBridgeTransferConnection" expr="'0'"/>
        </transition>

        <!-- SpeakTo Transfer Connection Handlers -->
        <transition event="connection.connected" cond="event$.connectionid == speakToTransferConnection">
            <log expr="'speakTo transfer: connection.connected for ' + event$.connectionid"/>
            <if cond="speakTo == 'a'">
                <!-- put speakTo (B) on hold now that it is connected-->
                <redirect connectionid="speakToTransferConnection" dest="session.connections[speakToTransferConnection].remote" hints="({hold: true})"/>
            </if>
        </transition>

        <transition event="connection.disconnected" cond="event$.connectionid == speakToTransferConnection">
            <log expr="'speakTo transfer: connection.disconnected for ' + event$.connectionid"/>
            <assign name="speakToTransferConnection" expr="'0'"/>
            <if cond="incomingConnection != '0' &amp;&amp; bridgeConnection != '0'">
                <!-- SpeakTo connection dropping out, return to normal 2 connection session -->
                <unjoin id1="speakToConference" id2="incomingConnectionProxy" hints="incomingConnectionProxyHints"/>  <!-- Workaround until TapProxy is fixed -->
                <join id1="bridgeConnection" id2="incomingConnectionProxy" duplex="'full'" hints="incomingConnectionProxyHints"/>
                <destroyconference conferenceid="speakToConference"/>
                <assign name="speakToConference" expr="'0'"/>
                <assign name="speakToTransferConnection" expr="'0'"/>
            <else/>
                <!-- Only one connection left, start the process of ending the session -->
                <assign name="speakToTransferConnection" expr="'0'"/>
                <if cond="incomingConnection == '0'">
                    <disconnect connectionid="bridgeConnection" hints="event$.info"/>
                    <assign name="bridgeConnection" expr="'0'"/>
                <else/>
                    <disconnect connectionid="incomingConnection" hints="event$.info"/>
                    <assign name="incomingConnection" expr="'0'"/>
                </if>
                <!-- Provide disconnected state -->
                <assign name="state" expr="'disconnected'"/>
                <assign name="disconnectType" expr="'implicit'"/>
                <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType"/>
                <!-- Terminate line recording if it is active -->
                <if cond="dialogRecord != '0'">
                    <dialogterminate dialogid="dialogRecord" immediate="false"/>
                </if>
            </if>
        </transition>

        <!-- SpeakTo Conference Handlers -->
        <transition event="conference.created" cond="event$.conferenceid == speakToConference">
            <log expr="'speakTo ' + speakTo + ' conference conference.created for ' + event$.conferenceid"/>
        </transition>

        <!-- Common Disconnect Confirmation Handler -->
        <transition event="connection.disconnected">
            <log expr="'connection.disconnected for ' + event$.connectionid"/>
            <if cond="dialogRecord == '0'">
                <!-- Session terminating due to disconnect confirmation -->
                <assign name="state" expr="'terminated'"/>
                <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType"/>
                <exit/>
            </if>
        </transition>

        <!-- Dialog Handlers -->        
        <transition event="dialog.started" cond="dialogHold == event$.dialogid">
            <log expr="'hold dialog ' + dialogHold + ' started'"/>
        </transition>

        <transition event="dialog.exit" cond="dialogHold == event$.dialogid">
            <log expr="'hold dialog ' + dialogHold + ' exited'"/>
        </transition>

        <transition event="error.dialog.*" cond="dialogHold == event$.dialogid">
            <log expr="'error has occurred on the hold dialog (' + event$.reason + ')'"/>
            <!-- non-fatal session error, results in a silent hold -->
            <assign name="dialogHold" expr="'0'"/>
        </transition>

        <transition event="dialog.started" cond="dialogRecord == event$.dialogid">
            <log expr="'record dialog ' + dialogRecord + ' started'"/>
        </transition>

        <transition event="dialog.exit" cond="dialogRecord == event$.dialogid">
            <log expr="'record dialog ' + dialogRecord + ' exited'"/>
            <var name="recordingFileUri" expr="event$.values.recordingFileUri"/>
            <!-- if all connections are disconnected then terminate session -->
            <if cond="incomingConnection == '0' &amp;&amp; speakToTransferConnection == '0' &amp;&amp; bridgeConnection == '0'">
                <assign name="state" expr="'terminated'"/>
                <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType recordingFileUri"/>
                <exit/>
            </if>
            <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType recordingFileUri"/>
            <assign name="dialogRecord" expr="'0'"/>
        </transition>

        <transition event="error.dialog.*" cond="dialogRecord == event$.dialogid">
            <log expr="'error has occurred on the record dialog (' + event$.reason + ')'"/>
            <!-- non-fatal session error, results in a silent hold -->
            <assign name="dialogRecord" expr="'0'"/>
        </transition>

        <!-- Cloud Operation Handlers -->
        <transition event="cloud.command.disconnect">
            <log expr="'disconnect request for ' + incomingConnection"/>
            <assign name="disconnectType" expr="event$.disconnectType"/>
            <!-- Disconnect the incoming connection and allow disconnect confirmation event to complete the disconnect sequence -->
            <disconnect connectionid="incomingConnection" hints="event$.disconnectReason"/>
        </transition>

        <!-- Error Handlers -->
        <transition event="error.*">
            <log expr="'error has occurred (' + event$.reason + ')'"/>
            <!-- Session is exiting due to error, move to a terminated state -->
            <assign name="state" expr="'terminated'"/>
            <assign name="disconnectType" expr="'explicit.error'"/>
            <var name="errorInfo" expr="({text: event$.reason})"/>
            <send name="'cloud.event.line.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType errorInfo"/>
            <exit/>
        </transition>

    </eventprocessor>

</ccxml>