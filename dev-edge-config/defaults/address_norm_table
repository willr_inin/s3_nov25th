[
    {
        "addressNormalization.priority": 1,
        "addressNormalization.enabled": true,
        "addressNormalization.matches": "^(911)$",
        "addressNormalization.label": "Emergency telephone number (country specific) such as 112, 911, 118, 119, 000, 110, 08, and 999",
        "addressNormalization.normalizedFormat": "$1",
        "addressNormalization.displayFormat": "$1",
        "addressNormalization.addressClassName": "Emergency"
    },
    {
        "addressNormalization.priority": 2,
        "addressNormalization.enabled": true,
        "addressNormalization.matches": "^(411)$",
        "addressNormalization.label": "Information telephone number (country specific).",
        "addressNormalization.normalizedFormat": "$1",
        "addressNormalization.displayFormat": "$1",
        "addressNormalization.addressClassName": "Information"
    },
    {
        "addressNormalization.priority": 3,
        "addressNormalization.enabled": true,
        "addressNormalization.matches": "^(tel:|sip:|sips:)?(\\d{4})(@.*)?$",
        "addressNormalization.label": "4-digit extensions",
        "addressNormalization.normalizedFormat": "$1+19196669800;ext=$2$3",
        "addressNormalization.displayFormat": "$1",
        "addressNormalization.addressClassName": "Intercom"
    },
    {
        "addressNormalization.priority": 4,
        "addressNormalization.enabled": true,
        "addressNormalization.matches": "^(tel:|sip:|sips:)?\\+?(1)?[-. ]?\\(?(919)?\\)?[-. ]?(\\d{3})[-. ]?(\\d{4})(@.*)?$",
        "addressNormalization.label": "Optionally formatted number with local area code",
        "addressNormalization.normalizedFormat": "$1+1919$4$5$6",
        "addressNormalization.displayFormat": "$4-$5",
        "addressNormalization.addressClassName": "Local"
    },
    {
        "addressNormalization.priority": 5,
        "addressNormalization.enabled": true,
        "addressNormalization.matches": "^(tel:|sip:|sips:)?\\+?(1)?[-. ]?\\(?(800|888|877|866|855|844|833|822)?\\)?[-. ]?(\\d{3})[-. ]?(\\d{4})(@.*)?$",
        "addressNormalization.label": "Optionally formatted number with toll free area codes.  Should be after any entries for local area code for least cost routing.",
        "addressNormalization.normalizedFormat": "$1+1$3$4$5$6",
        "addressNormalization.displayFormat": "($3) $4-$5",
        "addressNormalization.addressClassName": "Toll Free"
    },
    {
        "addressNormalization.priority": 6,
        "addressNormalization.enabled": true,
        "addressNormalization.matches": "^(tel:|sip:|sips:)?\\+?(1)?[-. ]?\\(?(\\d{3})?\\)?[-. ]?(\\d{3})[-. ]?(\\d{4})(@.*)?$",
        "addressNormalization.label": "Optionally formatted number with any area code.  Should be after any entries for exact area codes for least cost routing.",
        "addressNormalization.normalizedFormat": "$1+1$3$4$5$6",
        "addressNormalization.displayFormat": "($3) $4-$5",
        "addressNormalization.addressClassName": "Long Distance"
    },
    {
        "addressNormalization.priority": 7,
        "addressNormalization.enabled": true,
        "addressNormalization.matches": "^(tel:|sip:|sips:)?\\+?(\\d{1,3})?[-. ]?\\(?(((?!919)\\d)*)?\\)?[-. ]?(\\d{3})[-. ]?(\\d{4})(@.*)?$",
        "addressNormalization.label": "Optionally formatted number with any country code.  Should be after any entries for exact country codes for least cost routing.",
        "addressNormalization.normalizedFormat": "$1+$2$3$4$5$6",
        "addressNormalization.displayFormat": "($3) $4-$5",
        "addressNormalization.addressClassName": "International"
    }
]