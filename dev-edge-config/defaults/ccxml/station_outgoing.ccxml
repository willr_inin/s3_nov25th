<?xml version="1.0" encoding="UTF-8"?>
<!--
  $File: //edge/main_systest/pub/resources/documents/ccxml/station_outgoing.ccxml $
  $Revision: #3 $
  $Change: 918391 $
  $DateTime: 2013/11/07 10:44:52 $
  $Author: richardn $

  Copyright (c) Interactive Intelligence, Inc. 2012
  All Rights Reserved
 -->
<ccxml version="1.0">

    <!-- station_outgoing.ccxml
            Purpose:
                Processes a makecall operation to initate the session control. The session will first dial
                an outgoing connection to a station. Once connected, the session will dial an outgoing
                session bridge connection. The stationConnection is the primary resource of this session
                document and is the driver of all events to the cloud.

            Session Variables:
                None.

            Operations:
                cloud.command.makecall.station      : Initites the session process, first contacting the station
                cloud.command.disconnect            : Disconnects the bridge connection, response disconnect event disconnects the station which will then invoke the common disconnect handler
                cloud.command.mediaflow.mute        : Mutes the station connection audio, audio flows from bridge to station
                cloud.command.mediaflow.unmute      : Unmutes the station connection audio
                cloud.command.mediaflow.deafen      : Deafens the station connection audio, audio flows from station to bridge
                cloud.command.mediaflow.undeafen    : Undeafens the station connection audio
                cloud.command.hold                  : Puts the session on hold or remove it from hold, locally.
                cloud.command.transfer.attended     : Requests a transfer to the bridged tie line call to a new number or destination, station party remains until bridge disconnects it.
                cloud.command.transfer.unattended   : Requests a transfer to the bridged tie line call to a new number or destination, eliminating this station party.
                cloud.command.transfer.speakto      : Requests a transfer to the bridged tie line call to promote conversation to a 3-party conversation with speaking privliges.
     -->

    <var name="stationConnection" expr="'0'"/>          <!-- Outgoing or incoming connection with a station -->
    <var name="bridgeConnection" expr="'0'"/>           <!-- Bridge connection -->
    <var name="bridgeTransferConnection" expr="'0'"/>   <!-- Bridge Transfer connection, active while the transfer request is active -->
    <var name="oldBridgeTransferConnection" expr="'0'"/><!-- Bridge Transfer connection that is no longer active because it was disconnected and replaced -->
    <var name="dialogRingback" expr="'0'"/>             <!-- Dialog endpoint id for the ringback audio -->
    <var name="dialogDisconnect" expr="'0'"/>           <!-- Dialog endpoint id for the disconnect audio -->
    <var name="dialogHold" expr="'0'"/>                 <!-- Dialog endpoint id hold audio -->
    <var name="sendTarget" expr="'0'"/>                 <!-- Target Connection for all cloud send elements -->
    <var name="pendingAudioStatus" expr="'full'"/>      <!-- Pending audio.status when a join or unjoin has been issued -->
    <var name="aai" expr=""/>                           <!-- Station party's AAI -->
    <var name="direction" expr="'outbound'"/>           <!-- Cloud payload name: Direction of primary connection - constant -->
    <var name="state" expr="'contacting'"/>             <!-- Cloud payload name: State of session - initially in contacting state -->
    <var name="disconnectType" expr=""/>                <!-- Cloud payload name: Type of the disconnect -->
    <var name="audio" expr="({status: 'full'})"/>       <!-- Maintains state of the audio status combinations: none, mute, deafen, full -->
    <assign name="audio.hold" expr="({remote: false, local: false})"/> <!-- Maintains state of the audio hold -->

    <!-- cloud.cmd.makecall.external parameters -->
    <var name="makecall" expr=""/>

    <eventprocessor>

        <!-- Bridge Connection Handlers -->
        <transition event="connection.progressing" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.progressing for ' + event$.connectionid + ', substate is ' + event$.connection.substate"/>
            <if cond="event$.connection.substate.indexOf('.media') >= 0">
                <!-- Progressing status has media, stop ringback and cut audio path if needed -->
                <if cond="dialogRingback != '0'">
                    <join id1="bridgeConnection" id2="stationConnection" duplex="'full'"/>
                    <dialogterminate dialogid="dialogRingback" immediate="true"/>
                </if>
            <else/>
                <!-- Progressing status does not have media, start ringback to station if needed -->
                <if cond="dialogRingback == '0'">
                    <var name="station" expr="Edge.get_station(session.connections[stationConnection].remote);"/>
                    <var name="audioAttr" expr="station.get_config().query('station.resource.ringback')"/>
                    <if cond="audioAttr != undefined">
                        <dialogstart src="audioAttr.valueOf()" type="'audio/x-wav'" dialogid="dialogRingback" connectionid="stationConnection" mediadirection="both"/>
                    </if>
                </if>
            </if>
        </transition>

        <transition event="connection.signal.transfer" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.signal.transfer for ' + event$.connectionid"/>
            <if cond="event$.info.aai != undefined">
                <assign name="aai" expr="event$.info.aai"/>
            </if>
            <if cond="bridgeTransferConnection != '0'">
                <!-- already an active transfer, disconnect it and start a new one -->
                <disconnect connectionid="bridgeTransferConnection"/>
                <assign name="oldBridgeTransferConnection" expr="bridgeTransferConnection"/>
            </if>
            <createcall connectionid="bridgeTransferConnection"
                        hints="({dialplan: 'tieline', diversion: {address: session.connections[bridgeConnection].remote}})"
                        dest="event$.info.destination.address"
                        callerid="session.connections[stationConnection].remote"
                        aai="aai"
            />
        </transition>

        <transition event="connection.signal.hold" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.signal.hold = ' + event$.info.state + ' for ' + event$.connectionid"/>
            <assign name="audio.hold.remote" expr="event$.info.state"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
            <!-- Alter the hold audio to station connection -->
            <if cond="event$.info.state">
                <var name="station" expr="Edge.get_station(session.connections[stationConnection].remote);"/>
                <var name="srcAttr" expr="station.get_attribute('station.resource.hold')"/>
                <if cond="!(srcAttr.isNull || srcAttr.empty)">
                    <dialogstart src="'data:text/uri-list,' + 'x-inin-audiosrc:control/loop_begin%0D%0A' + srcAttr.valueOf() + '%0D%0Ax-inin-audiosrc:control/loop_end'"
                                 type="'audio/x-wav'"
                                 dialogid="dialogHold"
                                 connectionid="stationConnection"
                                 mediadirection="both"/>
                <else/>
                    <log expr="'remotely held without hold audio - station.resource.hold is not configured for ' + session.connections[stationConnection].remote"/>
                    <unjoin id1="bridgeConnection" id2="stationConnection"/>
                </if>
            <else/>
                <join id1="bridgeConnection" id2="stationConnection" duplex="'full'"/>
                <if cond="dialogHold != '0'">
                    <dialogterminate dialogid="dialogHold" immediate="true"/>
                </if>
            </if>
        </transition>

        <transition event="connection.connected" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.connected for ' + event$.connectionid"/>
            <!-- Stop ringback and cut audio path if needed -->
            <if cond="dialogRingback != '0'">
                <join id1="bridgeConnection" id2="stationConnection" duplex="'full'"/>
                <dialogterminate dialogid="dialogRingback" immediate="true"/>
            </if>
            <!-- Provide connected state -->
            <assign name="state" expr="'connected'"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
        </transition>

        <transition event="connection.redirected" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.redirected for ' + event$.connectionid"/>
            <assign name="disconnectType" expr="'explicit.transfer'"/>
            <!-- Provide disconnected state -->
            <assign name="state" expr="'disconnected'"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType audio"/>
            <!-- Disconnect the station connection -->
            <disconnect connectionid="stationConnection" hints="event$.info"/>
            <!-- Clear both connections and allow common disconnect handler to complete document -->
            <assign name="stationConnection" expr="'0'"/>
            <assign name="bridgeConnection" expr="'0'"/>
        </transition>

        <transition event="connection.disconnected" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.disconnected for ' + event$.connectionid"/>
            <if cond="bridgeTransferConnection != '0'">
                <!-- Original bridge has been disconnected during active transfer, complete the transfer -->
                <join id1="bridgeTransferConnection" id2="stationConnection" duplex="'full'"/>
                <assign name="bridgeConnection" expr="bridgeTransferConnection"/>
                <assign name="bridgeTransferConnection" expr="'0'"/>
            <else/>
                <!-- If type is already set do not overwrite its value, it was set by the cloud disconnect -->
                <if cond="!disconnectType">
                    <assign name="disconnectType" expr="'implicit'"/>
                </if>
                <!-- Provide disconnected state -->
                <assign name="state" expr="'disconnected'"/>
                <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType audio"/>
                <!-- Determine if audio needs to be played to the station -->
                <var name="station" expr="Edge.get_station(session.connections[stationConnection].remote);"/>
                <!-- TODO: replace event$.info.sip with event$.info and remove conditional check when IONCORE-650 is complete -->
                <!-- <var name="audioUri" expr="station.media_for_disconnect(new AttributeMap(event$.info))"/> -->
                <var name="audioUri" expr="''"/>
                <if cond="event$.info.sip != undefined">
                    <assign name="audioUri" expr="station.media_for_disconnect(new AttributeMap(event$.info.sip))"/>
                </if>
                <if cond="audioUri != ''">
                    <dialogstart src="audioUri" type="'audio/x-wav'" dialogid="dialogDisconnect" connectionid="stationConnection" mediadirection="both"/>
                <else/>
                    <!-- Disconnect the station connection -->
                    <disconnect connectionid="stationConnection" hints="event$.info"/>
                    <!-- Clear station connection and allow common disconnect handler to complete document -->
                    <assign name="stationConnection" expr="'0'"/>
                </if>
                <assign name="bridgeConnection" expr="'0'"/>
            </if>
        </transition>

        <transition event="connection.failed" cond="event$.connectionid == bridgeConnection">
            <log expr="'bridge: connection.failed for ' + event$.connectionid"/>
            <!-- Provide disconnected state then move immediately to a terminated state -->
            <assign name="state" expr="'disconnected'"/>
            <assign name="disconnectType" expr="'explicit.error'"/>
            <var name="errorInfo" expr="({text: event$.reason})"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType audio errorInfo"/>
            <!-- Disconnect the station connection -->
            <disconnect connectionid="stationConnection" hints="event$.info"/>
            <!-- Clear both connections and allow common disconnect handler to complete document -->
            <assign name="stationConnection" expr="'0'"/>
            <assign name="bridgeConnection" expr="'0'"/>
        </transition>

        <!-- Station Connection Handlers -->
        <transition event="connection.alerting" cond="stationConnection == '0'">
            <log expr="'station: connection.alerting for ' + event$.connectionid"/>
            <assign name="stationConnection" expr="event$.connectionid"/>
            <!-- Answer the incoming connection from the station -->
            <accept connectionid="stationConnection"/>
            <assign name="sendTarget" expr="stationConnection"/>
            <!-- Notify the cloud that station contacting has started -->
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
        </transition>

        <transition event="connection.connected" cond="event$.connectionid == stationConnection">
            <log expr="'station: connection.connected for ' + event$.connectionid"/>
            <if cond="makecall != undefined">
                <!-- Create the outgoing bridge call on behalf of this station connection using makecall parameters -->
                <log expr="'initiating makecall outgoing call to ' + makecall.destination.address"/>
                <createcall connectionid="bridgeConnection"
                            hints="({dialplan: 'tieline'})"
                            dest="makecall.destination.address"
                            callerid="makecall.origination.address"
                            joinid="stationConnection"
                            timeout="makecall.options.timeout + 's'"
                />
            <else/>
                <!-- Create the outgoing bridge call on behalf of this station connection using the station dialed parameters -->
                <log expr="'initiating station dialed outgoing call to ' + session.connections[event$.connectionid].local"/>
                <createcall connectionid="bridgeConnection"
                            hints="({dialplan: 'tieline'})"
                            dest="session.connections[event$.connectionid].local"
                            callerid="session.connections[event$.connectionid].remote"
                            joinid="stationConnection"
                />
            </if>
            <log expr="'created outgoing call ' + bridgeConnection"/>
            <!-- Station is connected, notifiy the cloud -->
            <assign name="state" expr="'dialing'"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
        </transition>

        <transition event="connection.signal.hold" cond="event$.connectionid == stationConnection">
            <log expr="'station: connection.signal.hold = ' + event$.info.state + ' for ' + event$.connectionid"/>
            <assign name="audio.hold.local" expr="event$.info.state"/>
            <redirect connectionid="bridgeConnection" dest="session.connections[event$.connectionid].local" hints="({hold: event$.info.state})"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
        </transition>

        <transition event="connection.disconnected" cond="event$.connectionid == stationConnection">
            <log expr="'station: connection.disconnected for ' + event$.connectionid"/>
            <!-- Provide disconnected state then move immediately to a terminated state -->
            <assign name="state" expr="'disconnected'"/>
            <assign name="disconnectType" expr="'explicit.endpoint'"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType audio"/>
            <!-- Disconnect the session bridge connection -->
            <if cond="bridgeConnection == '0'">
                <!-- Bridge connection may have not been created yet, early exit of session -->
                <assign name="state" expr="'terminated'"/>
                <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType"/>
                <exit/>
            <else/>
                <disconnect connectionid="bridgeConnection" hints="event$.info"/>
            </if>
            <!-- Clear both connections and allow common disconnect handler to complete document -->
            <assign name="stationConnection" expr="'0'"/>
            <assign name="bridgeConnection" expr="'0'"/>
        </transition>

        <transition event="connection.failed" cond="event$.connectionid == stationConnection">
            <log expr="'station: connection.failed for ' + event$.connectionid"/>
            <!-- Provide disconnected state then move immediately to a terminated state -->
            <assign name="state" expr="'disconnected'"/>
            <assign name="disconnectType" expr="'explicit.error'"/>
            <var name="errorInfo" expr="({text: event$.reason})"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType audio errorInfo"/>
            <!-- Disconnect the session bridge connection -->
            <if cond="bridgeConnection == '0'">
                <!-- Bridge connection may have not been created yet, early exit of session -->
                <assign name="state" expr="'terminated'"/>
                <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType"/>
                <exit/>
            <else/>
                <disconnect connectionid="bridgeConnection" hints="event$.info"/>
            </if>
            <!-- Clear both connections and allow common disconnect handler to complete document -->
            <assign name="stationConnection" expr="'0'"/>
            <assign name="bridgeConnection" expr="'0'"/>
        </transition>

        <!-- Bridge Transfer Connection Handlers -->
        <transition event="connection.connected" cond="event$.connectionid == bridgeTransferConnection">
            <log expr="'bridge transfer: connection.connected for ' + event$.connectionid"/>
            <!-- Signal success by joining up the new audio and replacing the original bridge with the transfer bridge -->
            <join id1="bridgeTransferConnection" id2="stationConnection" duplex="'full'"/>
            <if cond="bridgeConnection != '0'">
                <disconnect connectionid="bridgeConnection" hints="({sip: {code: 410}})"/>
            </if>
        </transition>

        <transition event="connection.disconnected" cond="event$.connectionid == bridgeTransferConnection">
            <log expr="'bridge transfer: connection.disconnected for ' + event$.connectionid"/>
            <!-- Transfer failed, clear the transfer connection -->
            <assign name="bridgeTransferConnection" expr="'0'"/>
            <if cond="bridgeConnection == '0'">
                <!-- Originating connection has already disconnected, need to end session -->
                <assign name="state" expr="'disconnected'"/>
                <assign name="disconnectType" expr="'implicit'"/>
                <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType"/>
                <!-- Disconnect the station connection -->
                <disconnect connectionid="stationConnection" hints="event$.info"/>
                <!-- Clear both connections and allow common disconnect handler to complete document -->
                <assign name="stationConnection" expr="'0'"/>
            </if>
        </transition>

        <!-- Old Bridge Transfer Connection Handlers -->
        <transition event="connection.disconnected" cond="event$.connectionid == oldBridgeTransferConnection">
            <log expr="'old bridge transfer: connection.disconnected for ' + event$.connectionid"/>
            <assign name="oldBridgeTransferConnection" expr="'0'"/>
        </transition>

        <!-- Common Disconnect Confirmation Handler -->
        <transition event="connection.disconnected">
            <log expr="'connection.disconnected for ' + event$.connectionid"/>
            <!-- Session terminating due to disconnect confirmation -->
            <assign name="state" expr="'terminated'"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType"/>
            <exit/>
        </transition>

        <!-- Dialog Handlers -->
        <transition event="dialog.started" cond="dialogDisconnect == event$.dialogid">
            <log expr="'disconnect dialog ' + dialogDisconnect + ' started'"/>
        </transition>

        <transition event="dialog.exit" cond="dialogDisconnect == event$.dialogid">
            <log expr="'disconnect dialog ' + dialogDisconnect + ' exited'"/>
            <!-- Disconnect the station connection -->
            <disconnect connectionid="stationConnection" hints=""/>
            <!-- Clear station connection and allow common disconnect handler to complete document -->
            <assign name="stationConnection" expr="'0'"/>
            <assign name="dialogDisconnect" expr="'0'"/>
        </transition>

        <transition event="dialog.started" cond="dialogRingback == event$.dialogid">
            <log expr="'ringback dialog ' + dialogRingback + ' started'"/>
        </transition>

        <transition event="dialog.exit" cond="dialogRingback == event$.dialogid">
            <log expr="'ringback dialog ' + dialogRingback + ' exited'"/>
            <assign name="dialogRingback" expr="'0'"/>
        </transition>

        <transition event="error.dialog.*" cond="dialogRingback == event$.dialogid">
            <log expr="'error has occurred on the ringback dialog (' + event$.reason + ')'"/>
            <!-- non-fatal session error -->
            <join id1="bridgeConnection" id2="stationConnection" duplex="'full'"/>
            <assign name="dialogRingback" expr="'0'"/>
        </transition>

        <transition event="dialog.started" cond="dialogHold == event$.dialogid">
            <log expr="'hold dialog ' + dialogHold + ' started'"/>
        </transition>

        <transition event="dialog.exit" cond="dialogHold == event$.dialogid">
            <log expr="'hold dialog ' + dialogHold + ' exited'"/>
            <assign name="dialogHold" expr="'0'"/>
        </transition>

        <transition event="error.dialog.*" cond="dialogHold == event$.dialogid">
            <log expr="'error has occurred on the hold dialog (' + event$.reason + ')'"/>
            <!-- non-fatal session error, results in a silent hold -->
            <assign name="dialogHold" expr="'0'"/>
        </transition>

        <!-- Cloud Operation Handlers -->
        <transition event="cloud.command.makecall.station">
            <log expr="'initiating station call to ' + event$.station.name"/>
            <assign name="makecall" expr="event$"/>
            <!-- Create the primary station connection to connect the outgoing session bridge to -->
            <createcall connectionid="stationConnection"
                        hints="({dialplan: 'station.outgoing', autoanswer: true})"
                        dest="makecall.station.name"
                        callerid="makecall.destination.address"
            />
            <log expr="'created station call ' + stationConnection"/>
            <assign name="sendTarget" expr="stationConnection"/>
            <!-- Notify the cloud that station contacting has started -->
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
        </transition>

        <transition event="cloud.command.disconnect">
            <log expr="'disconnect request for ' + bridgeConnection"/>
            <assign name="disconnectType" expr="event$.disconnectType"/>
            <!-- Disconnect the bridge connection and allow disconnect confirmation event to complete the disconnect sequence -->
            <disconnect connectionid="bridgeConnection" hints="event$.disconnectReason"/>
        </transition>

        <transition event="cloud.command.transfer.*">
            <log expr="event$.name + ' request for ' + stationConnection + ' to ' + event$.destination.address"/>
            <redirect connectionid="bridgeConnection" dest="event$.destination.address" reason="event$.diversion.reason" hints="event$"/>
            <!-- An unattended transfer immediately disconnects the connection which will result in the session ending -->
            <if cond="event$.name == 'cloud.command.transfer.unattended'">
                <assign name="disconnectType" expr="'explicit.transfer'"/>
                <!-- Provide disconnected state and allow common disconnect handler to move to terminated state -->
                <assign name="state" expr="'disconnected'"/>
                <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType"/>
                <!-- Disconnect the incoming session bridge connection -->
                <disconnect connectionid="bridgeConnection"/>
                <!-- Clear connection and allow common disconnect handler to complete document -->
                <assign name="bridgeConnection" expr="'0'"/>
            </if>
        </transition>

        <transition event="cloud.command.mediaflow.mute">
            <log expr="'applying mute, current audio state = ' + audio.status"/>
            <if cond="audio.status == 'full'">        <!-- full audio, go to mute -->
                <join id1="stationConnection" id2="bridgeConnection" duplex="'half'"/>
                <assign name="pendingAudioStatus" expr="'mute'"/>
            <elseif cond="audio.status == 'deafen'"/> <!-- already deafened, remove all audio -->
                <unjoin id1="stationConnection" id2="bridgeConnection"/>
                <assign name="pendingAudioStatus" expr="'none'"/>
            </if>
        </transition>

        <transition event="cloud.command.mediaflow.unmute">
            <log expr="'applying unmute, current audio state = ' + audio.status"/>
            <if cond="audio.status == 'mute'">        <!-- is muted, go back to full -->
                <join id1="stationConnection" id2="bridgeConnection" duplex="'full'"/>
                <assign name="pendingAudioStatus" expr="'full'"/>
            <elseif cond="audio.status == 'none'"/>   <!-- is muted and deafened, go to just deafen -->
                <join id1="bridgeConnection" id2="stationConnection" duplex="'half'"/>
                <assign name="pendingAudioStatus" expr="'deafen'"/>
            </if>
        </transition>

        <transition event="cloud.command.mediaflow.deafen">
            <log expr="'applying deafen, current audio state = ' + audio.status"/>
            <if cond="audio.status == 'full'">        <!-- full audio, go to deafen -->
                <join id1="bridgeConnection" id2="stationConnection" duplex="'half'"/>
                <assign name="pendingAudioStatus" expr="'deafen'"/>
            <elseif cond="audio.status == 'mute'"/> <!-- already muted, remove all audio -->
                <unjoin id1="stationConnection" id2="bridgeConnection"/>
                <assign name="pendingAudioStatus" expr="'none'"/>
            </if>
        </transition>

        <transition event="cloud.command.mediaflow.undeafen">
            <log expr="'applying undeafen, current audio state = ' + audio.status"/>
            <if cond="audio.status == 'deafen'">        <!-- is deafened, go back to full -->
                <join id1="stationConnection" id2="bridgeConnection" duplex="'full'"/>
                <assign name="pendingAudioStatus" expr="'full'"/>
            <elseif cond="audio.status == 'none'"/>   <!-- is muted and deafened, go to just muted -->
                <join id1="stationConnection" id2="bridgeConnection" duplex="'half'"/>
                <assign name="pendingAudioStatus" expr="'mute'"/>
            </if>
        </transition>

        <transition event="cloud.command.hold">
            <log expr="'applying hold = ' + event$.state"/>
            <assign name="audio.hold.local" expr="event$.state"/>
            <redirect connectionid="bridgeConnection" dest="session.connections[bridgeConnection].remote" hints="({hold: event$.state})"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
        </transition>

        <!-- join/unjoin handlers -->
        <transition event="conference.joined" cond="audio.status != pendingAudioStatus">
            <assign name="audio.status" expr="pendingAudioStatus"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
        </transition>

        <transition event="conference.unjoined" cond="audio.status != pendingAudioStatus">
            <assign name="audio.status" expr="pendingAudioStatus"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state audio"/>
        </transition>

        <!-- Error Handlers -->
        <transition event="error.*">
            <log expr="'error has occurred (' + event$.reason + ')'"/>
            <!-- Session is exiting due to error, move to a terminated state -->
            <assign name="state" expr="'terminated'"/>
            <assign name="disconnectType" expr="'explicit.error'"/>
            <var name="errorInfo" expr="({text: event$.reason})"/>
            <send name="'cloud.event.station.change'" targettype="'basichttp'" target="sendTarget" namelist="direction state disconnectType errorInfo"/>
            <exit/>
        </transition>

    </eventprocessor>

</ccxml>
