Detailed VXML generation report:

	Code version: 0
	Generation id: MACINODIAICCKAEDPGGFLHNIGBMPBJKG
	Customer: 55bc6be3-078c-4a49-a4e6-1e05776ed7e8
	Generation time: 2013/11/11 15:56:20
	Generation duration: 80206 ms

	status WARNING count: 0
	status FAILEDBUTALLOWED count: 0
	status PASSED count: 7
	status FAILED count: 0

	PASSED

	1. A menu needs at least 2 choices to be useful. This one has 2.

		Location:
			[processing]: Menu for 'english', 'espanol' -> Minimal menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:494)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:139)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:509)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:139)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[with hash value of]: -1219850385
			
	2. A menu must have at most 7 choices to stay reasonable for a caller to remember (5 often recommended). This one has 2.

		Location:
			[processing]: Menu for 'english', 'espanol' -> Maximum menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:494)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:139)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:533)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:139)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[with hash value of]: -1552075218
			
	3. A menu needs at least 2 choices to be useful. This one has 4.

		Location:
			[processing]: Menu for 'sales', 'support', 'marketing', 'disconnect' -> Minimal menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:494)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:141)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:509)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:141)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[with hash value of]: 678468268
			
	4. A menu must have at most 7 choices to stay reasonable for a caller to remember (5 often recommended). This one has 4.

		Location:
			[processing]: Menu for 'sales', 'support', 'marketing', 'disconnect' -> Maximum menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:494)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:141)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:533)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:141)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[with hash value of]: 346243435
			
	5. A menu needs at least 2 choices to be useful. This one has 2.

		Location:
			[processing]: Menu for 'East coast sales', 'West coast sales' -> Minimal menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:494)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:143)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:509)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:143)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[with hash value of]: -1182463269
			
	6. A menu must have at most 7 choices to stay reasonable for a caller to remember (5 often recommended). This one has 2.

		Location:
			[processing]: Menu for 'East coast sales', 'West coast sales' -> Maximum menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:494)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:143)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:533)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:446)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:404)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:368)
			com.inin.generator.vxmlgen.VXMLGenAdapter.createQ3DemoMultipleSkillsAndCloudDips(VXMLGenAdapter.java:143)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:78)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[with hash value of]: -1514688102
			
	7. The URL 'http://55bc6be3-078c-4a49-a4e6-1e05776ed7e8.inin.com/clouddipservice' is valid.

		Location:
			[at]: com.inin.vxml.schemas.adapters.DataTagAdapter.setSrc
			com.inin.vxml.schemas.DataTag.createNew
			com.inin.vxml.vxmlgenapp.CloudDipSubdialogDocument.build
			com.inin.vxml.vxmlgenapp.ApplicationVxmlDocument.buildApplication
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.schemas.adapters.DataTagAdapter.setSrc(DataTagAdapter.java:36)
			com.inin.vxml.schemas.DataTag.createNew(DataTag.java:89)
			com.inin.vxml.vxmlgenapp.CloudDipSubdialogDocument.build(CloudDipSubdialogDocument.java:97)
			com.inin.vxml.vxmlgenapp.ApplicationVxmlDocument.buildApplication(ApplicationVxmlDocument.java:78)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:81)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1588)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.schemas.adapters.DataTagAdapter.setSrc(DataTagAdapter.java:48)
			com.inin.vxml.schemas.DataTag.createNew(DataTag.java:89)
			com.inin.vxml.vxmlgenapp.CloudDipSubdialogDocument.build(CloudDipSubdialogDocument.java:97)
			com.inin.vxml.vxmlgenapp.ApplicationVxmlDocument.buildApplication(ApplicationVxmlDocument.java:78)
			com.inin.generator.vxmlgen.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:81)
			com.inin.generator.vxmlgen.VxmlGeneratorServiceImpl.createVxml(VxmlGeneratorServiceImpl.java:222)
			com.inin.generator.vxmlgen.sqs.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:55)
			com.inin.generator.vxmlgen.sqs.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:63)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:744)
			[with hash value of]: 877572949
			

