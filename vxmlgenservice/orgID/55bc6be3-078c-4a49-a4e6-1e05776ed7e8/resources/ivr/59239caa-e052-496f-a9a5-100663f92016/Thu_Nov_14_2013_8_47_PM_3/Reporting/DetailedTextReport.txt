Detailed VXML generation report:

	Code version: 0
	Generation id: LPIJPCAAEGLIKPMFAPALHGGMMJFPIGHM
	Customer: 55bc6be3-078c-4a49-a4e6-1e05776ed7e8
	Generation time: 2013/11/14 20:47:57
	Generation duration: 455 ms

	status WARNING count: 0
	status FAILEDBUTALLOWED count: 0
	status PASSED count: 8
	status FAILED count: 0

	PASSED

	1. A menu needs at least 2 choices to be useful. This one has 2.

		Location:
			[processing]: Menu for 'english', 'espanol' -> Minimal menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:483)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:129)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:498)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:129)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[with hash value of]: -1219850385
			
	2. A menu must have at most 7 choices to stay reasonable for a caller to remember (5 often recommended). This one has 2.

		Location:
			[processing]: Menu for 'english', 'espanol' -> Maximum menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:483)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:129)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:522)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:129)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[with hash value of]: -1552075218
			
	3. A menu needs at least 2 choices to be useful. This one has 4.

		Location:
			[processing]: Menu for 'sales', 'support', 'marketing', 'disconnect' -> Minimal menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:483)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:131)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:498)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:131)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[with hash value of]: 678468268
			
	4. A menu must have at most 7 choices to stay reasonable for a caller to remember (5 often recommended). This one has 4.

		Location:
			[processing]: Menu for 'sales', 'support', 'marketing', 'disconnect' -> Maximum menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:483)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:131)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:522)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:131)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[with hash value of]: 346243435
			
	5. The URL 'http://www.nowhere.com/idontcare/' is valid.

		Location:
			[processing]: Menu for 'sales', 'support', 'marketing', 'disconnect' -> support -> data tag construction in menuchoicegroup2.vxml/MenuFormEntry + #1
			[at]: com.inin.vxml.schemas.adapters.DataTagAdapter.setSrc
			com.inin.vxml.schemas.DataTag.createNew
			com.inin.vxml.vxmlgenapp.interactions.input.IVRActionHandling.insertActionTags
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceItem.improveFilledTagIntoFieldTag
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.schemas.adapters.DataTagAdapter.setSrc(DataTagAdapter.java:36)
			com.inin.vxml.schemas.DataTag.createNew(DataTag.java:89)
			com.inin.vxml.vxmlgenapp.interactions.input.IVRActionHandling.insertActionTags(IVRActionHandling.java:420)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceItem.improveFilledTagIntoFieldTag(MenuChoiceItem.java:400)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:676)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:131)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.schemas.adapters.DataTagAdapter.setSrc(DataTagAdapter.java:48)
			com.inin.vxml.schemas.DataTag.createNew(DataTag.java:89)
			com.inin.vxml.vxmlgenapp.interactions.input.IVRActionHandling.insertActionTags(IVRActionHandling.java:420)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceItem.improveFilledTagIntoFieldTag(MenuChoiceItem.java:400)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:676)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:131)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[with hash value of]: 400427973
			
	6. The URL 'http://www.nowhere.com/idontcare/' is valid.

		Location:
			[processing]: Menu for 'sales', 'support', 'marketing', 'disconnect' -> marketing -> data tag construction in menuchoicegroup2.vxml/MenuFormEntry + #2
			[at]: com.inin.vxml.schemas.adapters.DataTagAdapter.setSrc
			com.inin.vxml.schemas.DataTag.createNew
			com.inin.vxml.vxmlgenapp.interactions.input.IVRActionHandling.insertActionTags
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceItem.improveFilledTagIntoFieldTag
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.schemas.adapters.DataTagAdapter.setSrc(DataTagAdapter.java:36)
			com.inin.vxml.schemas.DataTag.createNew(DataTag.java:89)
			com.inin.vxml.vxmlgenapp.interactions.input.IVRActionHandling.insertActionTags(IVRActionHandling.java:420)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceItem.improveFilledTagIntoFieldTag(MenuChoiceItem.java:400)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:676)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:131)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.schemas.adapters.DataTagAdapter.setSrc(DataTagAdapter.java:48)
			com.inin.vxml.schemas.DataTag.createNew(DataTag.java:89)
			com.inin.vxml.vxmlgenapp.interactions.input.IVRActionHandling.insertActionTags(IVRActionHandling.java:420)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceItem.improveFilledTagIntoFieldTag(MenuChoiceItem.java:400)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:676)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:131)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[with hash value of]: 1840581327
			
	7. A menu needs at least 2 choices to be useful. This one has 2.

		Location:
			[processing]: Menu for 'East coast sales', 'West coast sales' -> Minimal menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:483)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:133)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:498)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:133)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[with hash value of]: -1182463269
			
	8. A menu must have at most 7 choices to stay reasonable for a caller to remember (5 often recommended). This one has 2.

		Location:
			[processing]: Menu for 'East coast sales', 'West coast sales' -> Maximum menu options test
			[at]: com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build
			[related to code]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:483)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:133)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[full call-stack]: java.lang.Thread.getStackTrace(Thread.java:1567)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:47)
			com.inin.vxml.vxmlgenapp.utils.LocationInCode.<init>(LocationInCode.java:43)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:522)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:436)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.internalbuild(MenuChoiceGroup.java:395)
			com.inin.vxml.vxmlgenapp.interactions.input.menus.MenuChoiceGroup.build(MenuChoiceGroup.java:366)
			com.inin.architect.dsl.convert.VXMLGenAdapter.createNov19Demo(VXMLGenAdapter.java:133)
			com.inin.architect.dsl.convert.VXMLGenAdapter.processContext(VXMLGenAdapter.java:83)
			com.inin.architect.dsl.convert.VXMLGenAdapter.generateVxml(VXMLGenAdapter.java:65)
			com.inin.architect.dsl.service.VXMLGeneratorService.createVxml(VXMLGeneratorService.java:153)
			com.inin.architect.dsl.service.GenerateVxmlRequestHandler.handleMessage(GenerateVxmlRequestHandler.java:60)
			com.inin.architect.dsl.service.VxmlRequestConsumer$1.run(VxmlRequestConsumer.java:65)
			java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
			java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
			java.lang.Thread.run(Thread.java:722)
			[with hash value of]: -1514688102
			

